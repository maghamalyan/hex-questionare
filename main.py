import csv
import curses

from pick import Picker
from questionnaire import Questionnaire
from colorama import init
from colorama import Fore, Back, Style

import signal

# signal.signal(signal.SIGINT, signal.SIG_IGN)
# signal.signal(signal.SIGTSTP, signal.SIG_IGN)

data_file_name = "results.csv"


# def new_config_curses(self):
#     # use the default colors of the terminal
#     curses.use_default_colors()
#     # hide the cursor
#     curses.curs_set(0)
#     # add some color for multi_select
#     # @todo make colors configurable
#     curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)
#
#
# Picker.config_curses = new_config_curses


def main():
    while True:
        print()
        input("Hi there! \nPress enter to continue!")
        q = Questionnaire()

        q.raw('user', prompt='Username:')
        q.one('day', 'monday', 'friday', 'saturday', prompt='What day is it?')
        q.one('time', ('morning', 'in the morning'), ('night', 'at night'), prompt='What time is it?')

        q.run()
        with open(data_file_name, 'a') as data_file:
            writer = csv.writer(data_file)

            writer.writerow(q.answers.values())
        print()
        print("Thank you! We got your answer!")


if __name__ == '__main__':
    init()
    print(Fore.GREEN)
    print(Style.BRIGHT)
    print(Back.BLACK)
    try:
        main()
    except KeyboardInterrupt:
        password = input("Do you really want to exit?")
        if password == "Yep!":
            exit()

        print("Not really!")
        main()
