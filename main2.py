import colorama
import os

from session import Session

apr_url = "https://opentdb.com/api.php?amount=10&category=18"
data_file_base_name = "results_"
dragon_names = {
    "easy": "junior_dragon.txt",
    "medium": "average_dragon.txt",
    "hard": "chuck_norris.txt",
}


def get_file_name():
    i = 0
    while os.path.isfile(data_file_base_name + str(i) + ".csv"):
        i += 1

    return data_file_base_name + str(i) + ".csv"


def take_questionare(file_name):
    session = Session(dragon_names, apr_url)
    session.start()
    session.greet()

    session.ask_questions()

    session.show_score_and_farewell()
    session.save_results(file_name)


def main():
    file_name = get_file_name()
    print(colorama.ansi.clear_screen())
    while True:
        take_questionare(file_name)


if __name__ == "__main__":
    main()
