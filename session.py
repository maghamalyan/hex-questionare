from whaaaaat import prompt
import signal
import random
from random import shuffle
import os
import csv
import requests
import html
import colorama
from colorama import Fore, Back, Style


def handle_exit():
    exit_confirm = input("Are you sure you want to exit?")
    if exit_confirm == "Yup!":
        exit()


def make_unexitable():
    try:
        signal.signal(signal.SIGINT, signal.SIG_IGN)
        signal.signal(signal.SIGTSTP, signal.SIG_IGN)
        signal.signal(signal.SIGTERM, signal.SIG_IGN)
    except AttributeError:
        pass


def init_screen():
    colorama.init()
    print(Fore.GREEN)
    print(Style.BRIGHT)
    print(Back.BLACK)


def map_questions(response_questions):
    questions = []
    i = 0
    for question in response_questions:
        choices = question["incorrect_answers"] + [question["correct_answer"]]
        shuffle(choices)
        questions.append({
            'type': 'list',
            'name': f'q{i}',
            'message': html.unescape(question["question"]),
            'choices': [{'name': html.unescape(i), 'value': i} for i in choices],
            'defalut': 0,
        })
        i += 1
    return questions


def get_message_for_score(score):
    messages = [
        [
            "If you end your training now — if you choose the quick and easy path as Vader did — you will become an agent of evil.",
            "You must unlearn what you have learned."
        ],
        [
            "Much to learn you still have…my old padawan.",
            "This is just the beginning!",
            "PATIENCE YOU MUST HAVE my young padawan"
        ],
        [
            "Powerful you have become, the dark side I sense in you."
        ],
        [
            "Truly wonderful the mind of yours is."
        ]
    ]

    if score <= 3:
        return f"You have scored: {score} \n" + random.choice(messages[0])
    if score <= 5:
        return f"You have scored: {score} \n" + random.choice(messages[1])
    if score <= 7:
        return f"You have scored: {score} \n" + random.choice(messages[2])

    return f"You have scored: {score} \n" + random.choice(messages[3])


class Session(object):
    def __init__(self, dragon_names, apr_url):
        self.dragon_names = dragon_names
        self.apr_url = apr_url

        self.greet_answers = None
        self.original_questions = None
        self.question_answers = None
        self.score = None

    def start(self):
        make_unexitable()
        init_screen()

    def greet(self):
        while True:
            ready = input("Want to try your luck and accept the challenge? (yes/no) ")
            if ready.lower().replace("!", "") == "yes":
                break
        print(colorama.ansi.clear_screen())
        greet_questions = [
            {
                'type': 'input',
                'name': 'first_name',
                'message': 'What\'s your first name',
            },
            {
                'type': 'input',
                'name': 'last_name',
                'message': 'What\'s your last name',
            },
            {
                'type': 'list',
                'name': 'complexity',
                'message': 'Choose your dragon to fight!',
                'choices': [
                    {'name': 'Dragon Egg', 'value': 'easy'},
                    {'name': 'Average Dragon', 'value': 'medium'},
                    {'name': 'Chuck Norris', 'value': 'hard'},
                ],
                'defalut': 0,
            }
        ]
        answers = prompt(greet_questions)

        with open(self.dragon_names[answers["complexity"]]) as dragon:
            print(dragon.read())

        self.greet_answers = answers

        return answers

    def get_complexity(self):
        return self.greet_answers["complexity"]

    def get_questions(self):
        url = self.apr_url + f"&difficulty={self.get_complexity()}"
        response = requests.get(url)
        response_questions = (response.json())["results"]
        return response_questions

    def ask_questions(self):
        print("Let the challenge begin!")

        self.original_questions = self.get_questions()
        self.question_answers = prompt(map_questions(self.original_questions))

    def show_score_and_farewell(self):
        score = 0

        for i in range(len(self.original_questions)):
            answer = self.question_answers[f"q{i}"]
            question = self.original_questions[i]

            if answer == question["correct_answer"]:
                score += 1

        print(get_message_for_score(score))
        self.score = score

    def save_results(self, data_file_name):
        if not os.path.isfile(data_file_name):
            with open(data_file_name, 'a') as data_file:
                writer = csv.writer(data_file)
                writer.writerow(['first_name', 'last_name', 'complexity', 'score'])

        with open(data_file_name, 'a') as data_file:
            writer = csv.writer(data_file)
            writer.writerow(list(self.greet_answers.values()) + [self.score])
